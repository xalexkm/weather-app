import { HttpClientModule } from '@angular/common/http';
import { Quote } from '@angular/compiler';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { AppComponent } from './app.component';
import { QuotesService } from './services/quotes.service';

describe('AppComponent', () => {
  let mockWeatherService = jasmine.createSpyObj(['getWeather']);
  let mockQuotesService = jasmine.createSpyObj(['getQuote']);
  let mockTextManipulationService = jasmine.createSpyObj(['splitString']);

  let component = new AppComponent(
    mockWeatherService,
    mockQuotesService,
    mockTextManipulationService
  );
  let mockFetchQuote = jasmine.createSpy('fetchQuote', component.fetchQuote);
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  let service: QuotesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [AppComponent],
      providers: [QuotesService],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    service = TestBed.inject(QuotesService);
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should call fetchQuote on initialisation', () => {
    // let spy = spyOn(service, 'getQuote').and.returnValue(
    //   timer(1000).pipe(mapTo({}))
    // );
    let spy = spyOn(app, 'fetchQuote');
    app.ngOnInit();
    expect(app.fetchQuote).toHaveBeenCalled();
  });
  it('should create div with wth--main class', waitForAsync(() => {
    fixture.whenStable().then(() => {
      const main = fixture.debugElement.queryAll(By.css('.wth--main'));
      expect(main[0].nativeElement.textContent).toBeTruthy();
    });
  }));
});
