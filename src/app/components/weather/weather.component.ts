import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { UnitConverterService } from '../../services/unit-converter.service';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'wth-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
})
export class WeatherComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() weatherData!: any;
  public temp!: number;
  public pressure!: string;
  public wind!: string;
  public location!: string;
  public center!: google.maps.LatLngLiteral;
  public mapOptions: google.maps.MapOptions = {
    disableDefaultUI: true,
    gestureHandling: 'greedy',
    keyboardShortcuts: false,
  };

  zoom = 7;
  constructor(private UnitConverterService: UnitConverterService) {}

  scrollToComponent(): void {
    window.scroll({
      top: 1000,
      behavior: 'smooth',
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.scrollToComponent();
  }
  ngOnChanges(): void {
    if (this.weatherData !== undefined) {
      this.temp = this.UnitConverterService.KelvinToCelsius(
        this.weatherData.main.temp
      );
      this.wind = this.UnitConverterService.MsToKMh(
        this.weatherData.wind.speed
      );
      this.location =
        this.weatherData.name + ', ' + this.weatherData.sys.country;
      this.pressure = this.weatherData.main.pressure;
      this.center = {
        lat: this.weatherData.coord.lat,
        lng: this.weatherData.coord.lon,
      };
    }
  }
}
