import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { QuotesService } from './services/quotes.service';
import { TextManipulationService } from './services/text-manipulation.service';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'wth-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public quoteSub!: Subscription;
  public weatherSub!: Subscription;
  public quote!: string[][] | undefined;
  public weather!: string;
  public weatherData!: any;
  public temp!: number;
  public charCount: number[] = [];
  public searchInput = new FormControl('');
  constructor(
    private quotesService: QuotesService,
    private weatherService: WeatherService,
    private textManipulationService: TextManipulationService
  ) {}

  fetchWeather(city: string) {
    this.weatherSub = this.weatherService.getWeather(city).subscribe({
      next: (data: any) => {
        this.weatherData = data;
      },
      complete: () => {
        window.scroll({
          top: 300,
          behavior: 'smooth',
        });
      },
    });
  }
  fetchQuote() {
    this.quoteSub = this.quotesService.getQuote().subscribe({
      next: (data) => {
        this.quote = this.textManipulationService.splitString(
          data.results[0].quote
        );
      },
    });
  }

  ngOnInit(): void {
    this.fetchQuote();
  }
}
