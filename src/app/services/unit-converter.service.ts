import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UnitConverterService {
  constructor() {}

  KelvinToCelsius(value: number | string) {
    if (typeof value === 'string') {
      return Math.round(parseInt(value) - 273.15);
    }
    return Math.round(value - 273.15);
  }
  MsToKMh(value: number | string) {
    if (typeof value === 'string') {
      return (parseInt(value) * 3.6).toFixed(1);
    }
    return (value * 3.6).toFixed(1);
  }
}
