import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TextManipulationService {
  constructor() {}
  splitString(str: string) {
    let words = str.split(' ');
    let arr: string[][] = [];
    let wordArray: string[] = [];
    for (let i = 0; i < words.length; i++) {
      let letters = [...words[i]];
      for (let i = 0; i < letters.length; i++) {
        wordArray.push(letters[i]);
      }
      arr.push(wordArray);
      letters = [];
      wordArray = [];
    }
    console.log(arr);
    return arr;
  }
}
