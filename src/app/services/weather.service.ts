import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  public api_key: string = '911ed8738b1b0703d6925925a207a2e9';
  public weatherUrl: string = `https://api.openweathermap.org/data/2.5/weather?q=Manchester&appid=${this.api_key}`;
  constructor(private http: HttpClient) {}

  getWeather(city: string) {
    return this.http.get<any>(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${this.api_key}`
    );
  }
}
