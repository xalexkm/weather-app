import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class QuotesService {
  public api_key: string = '0f1c1be9ddd649436098664c07122f995f908d48';
  public quotesUrl: string = `https://api.paperquotes.com/apiv1/quotes/?limit=1&maxlength=120&minlength=20&offset=${Math.floor(
    Math.random() * (300 - 0) + 0
  )}`;

  constructor(private http: HttpClient) {}

  getQuote() {
    return this.http
      .get<any>(this.quotesUrl, {
        headers: new HttpHeaders().set(
          'Authorization',
          `Token ${this.api_key}`
        ),
      })
      .pipe(
        tap((data) => console.log(data.results[0].quote)),
        catchError(() => of())
      );
  }
}
