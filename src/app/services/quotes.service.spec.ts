import { HttpClientModule } from '@angular/common/http';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';

import { QuotesService } from './quotes.service';

describe('QuotesService', () => {
  let service: QuotesService;
  let fakeData: Object = { results: [{ quote: 'This is a basic quote' }] };

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    service = TestBed.inject(QuotesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get an object with a quote', waitForAsync(() => {
    let spy = spyOn(service, 'getQuote').and.returnValue(
      timer(1000).pipe(mapTo(fakeData))
    );
    service.getQuote().subscribe({
      next: (data) => {
        expect(data).toEqual(fakeData);
      },
    });
    expect(service.getQuote);
  }));
});
