import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { UnitConverterService } from './unit-converter.service';

describe('UnitConverterService', () => {
  let sut: any;
  let service: UnitConverterService;
  let result: any;
  beforeEach(() => {
    sut = {};
    // TestBed.configureTestingModule({ imports: [HttpClientModule] });
    // service = TestBed.inject(UnitConverterService);
    service = new UnitConverterService();
    result = undefined;
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should display 10 if the received number is 283.15', () => {
    result = service.KelvinToCelsius(283.15);
    expect(result).toBe(10);
  });
  it('should display -20 if the received number is 253.15', () => {
    result = service.KelvinToCelsius(253.15);
    expect(result).toBe(-20);
  });
  it('should display 10 if the received string is 283.15', () => {
    result = service.KelvinToCelsius('283.15');
    expect(result).toBe(10);
  });
  it('should display -20 if the received string is 253.15', () => {
    result = service.KelvinToCelsius('253.15');
    expect(result).toBe(-20);
  });
});
