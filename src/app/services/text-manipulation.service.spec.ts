import { getSafePropertyAccessString } from '@angular/compiler';
import { TestBed } from '@angular/core/testing';

import { TextManipulationService } from './text-manipulation.service';

describe('TextManipulationService', () => {
  let service: TextManipulationService;
  let source: string = 'One quote';
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextManipulationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a quote array of word arrays of letters', () => {
    let result = service.splitString(source);
    expect(result).toEqual([
      ['O', 'n', 'e'],
      ['q', 'u', 'o', 't', 'e'],
    ]);
  });
});
